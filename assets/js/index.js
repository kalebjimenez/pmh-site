$(document).ready(function() {

 $('.vesicat-slider').slick({
   prevArrow: '<span class="arrow-left"><img src="img/arrow-left.png" alt=""></span>',
   nextArrow: '<span class="arrow-right"><img src="img/arrow-right.png" alt=""></span>'
 });
 $('.home-slider').slick({
   prevArrow: '<span class="arrow-left"><img src="img/arrow-left.png" alt=""></span>',
   nextArrow: '<span class="arrow-right"><img src="img/arrow-right.png" alt=""></span>'
 });
 $('.product-slider').slick({
   prevArrow: '<span class="arrow-left"><img src="img/arrow-left.png" alt=""></span>',
   nextArrow: '<span class="arrow-right"><img src="img/arrow-right.png" alt=""></span>'
 });

   var products = [
     {
       name:'TEGADERM + PAD',
       brand: '3M',
       category: 'CUIDADO DE HERIDAS',
       description: 'Apósito estéril transparente con almohadilla absorbente ',
       image_url: '/img/products/TEGADERM-PAD.jpg',
       product_url: '/tegaderm.html'
     },
     {
       name:'CAVILON No Sting Barrier',
       brand: '3M',
       category: 'CUIDADO DE HERIDAS',
       description: 'Película Protectora sin Ardor',
       image_url: '/img/products/CAVILON.jpg',
       product_url: '/cavilon.html'
     },
     {
       name:'TRANSPORE',
       brand: '3M',
       category: 'CINTAS MÉDICAS',
       description: 'Cinta Plástica Microperforada',
       image_url: '/img/products/TRANSPORE.png',
       product_url: '/transpore.html'
     },
     {
       name:'MICROPORE',
       brand: '3M',
       category: 'CINTAS MÉDICAS',
       description: 'Cinta adhesiva de papel',
       image_url: '/img/products/MICROPORE.jpg',
       product_url: '/micropore.html'
     },
     {
       name:'SCOTCHCAST PREMIUM Blanco',
       brand: '3M',
       category: 'CINTAS MÉDICAS',
       description: 'Vendas de fibra de vidrio en rollo',
       image_url: '/img/products/SCOTCHCAST.jpg',
       product_url: '/scotchcast.html'
     },
     {
       name:'STERIGAS Cartucho Dosis única',
       brand: '3M',
       category: 'ESTERILIZACIÓN',
       description: 'Cartucho de Oxido de Etileno ',
       image_url: '/img/products/STERIGAS.jpg',
       product_url: '/sterigas.html'
     },
     {
       name:'MINISPIKE',
       brand: 'B.BRAUN',
       category: 'PREPARACIÓN DE MEDICAMENTOS',
       description: 'Dispositivo para inyección y extracción segura de  fluidos.  Medicamentos no tóxicos',
       image_url: '/img/products/MINISPIKE.jpg',
       product_url: '/minispike.html'
     },
     {
       name:'ATTEST Paquete de prueba para vapor',
       brand: 'ETHICON',
       category: 'ESTERILIZACIÓN',
       description: 'Paquete de prueba Indicador biológico de lectura rápida (3 horas)',
       image_url: '/img/products/ATTEST.jpg',
       product_url: '/attest.html'
     },
     {
       name:'AVAGARD CHG 1%',
       brand: '3M',
       category: 'HIGIENE DE MANOS',
       description: 'Solución antiséptica para el lavado prequirúrgico de manos',
       image_url: '/img/products/AVAGARD.jpg',
       product_url: '/avagard-cgh.html'
     }
   ];

   var item_template =
    '<div class="product-item">' +
      '<img src="<%= obj.image_url %>" alt="" class="product-item__img img-responsive">' +
      '<h3 class="product-item__title"><%= obj.name %></h3>' +
      '<hr class="product-item__hr">' +
      '<% if (obj.brand) {  %><p class="product-item__brand"><%= obj.brand %><% } %></p>' +
      '<% if (obj.category) {  %><p class="product-item__category"><%= obj.category %><% } %></p>' +
      '<p><a href="<%= obj.product_url %>" class="product-item__button">Conoce Más</a></p>' +
    '</div>';


   var settings = {
     items           : products,
     facets          : {
                         'category' : 'Categoría',
                         'brand'    : 'Marca'
     },
     resultSelector  : '.product-grid',
     facetSelector   : '.product-filter',
     resultTemplate  : item_template,
     orderByOptions  : {'category': 'Categoría', 'brand': 'Marca'}
   }

   $(function(){
     $.facetelize(settings);
   });

});
