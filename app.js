const htmlStandards = require('reshape-standard')
const cssStandards = require('spike-css-standards')
const jsStandards = require('spike-js-standards')
const pageId = require('spike-page-id')
const env = process.env.SPIKE_ENV
const json = {
  items:[
    {
      name:'TEGADERM + PAD',
      brand: '3M',
      category: 'CUIDADO DE HERIDAS',
      description: 'Apósito estéril transparente con almohadilla absorbente ',
      image_url: '/img/products/TEGADERM-PAD.jpg',
      product_url: '/productos/test.html'
    },
    {
      name:'CAVILON No Sting Barrier',
      brand: '3M',
      category: 'CUIDADO DE HERIDAS',
      description: 'Película Protectora sin Ardor',
      image_url: '/img/products/CAVILON.jpg',
      product_url: '/productos/test.html'
    },
    {
      name:'TRANSPORE',
      brand: '3M',
      category: 'CINTAS MÉDICAS',
      description: 'Cinta Plástica Microperforada',
      image_url: '/img/products/TRANSPORE.png',
      product_url: '/productos/test.html'
    },
    {
      name:'MICROPORE',
      brand: '3M',
      category: 'CINTAS MÉDICAS',
      description: 'Cinta adhesiva de papel',
      image_url: '/img/products/MICROPORE.jpg',
      product_url: '/productos/test.html'
    },
    {
      name:'SCOTCHCAST PREMIUM Blanco',
      brand: '3M',
      category: 'CINTAS MÉDICAS',
      description: 'Vendas de fibra de vidrio en rollo',
      image_url: '/img/products/SCOTCHCAST.jpg',
      product_url: '/productos/test.html'
    },
    {
      name:'STERIGAS Cartucho Dosis única',
      brand: '3M',
      category: 'ESTERILIZACIÓN',
      description: 'Cartucho de Oxido de Etileno ',
      image_url: '/img/products/STERIGAS.jpg',
      product_url: '/productos/test.html'
    },
    {
      name:'MINISPIKE',
      brand: 'B.BRAUN',
      category: 'PREPARACIÓN DE MEDICAMENTOS',
      description: 'Dispositivo para inyección y extracción segura de  fluidos.  Medicamentos no tóxicos',
      image_url: '/img/products/MINISPIKE.jpg',
      product_url: '/productos/test.html'
    },
    {
      name:'ATTEST Paquete de prueba para vapor',
      brand: 'ETHICON',
      category: 'ESTERILIZACIÓN',
      description: 'Paquete de prueba Indicador biológico de lectura rápida (3 horas)',
      image_url: '/img/products/ATTEST.jpg',
      product_url: '/productos/test.html'
    },
    {
      name:'AVAGARD CHG 1%',
      brand: '3M',
      category: 'HIGIENE DE MANOS',
      description: 'Solución antiséptica para el lavado prequirúrgico de manos',
      image_url: '/img/products/AVAGARD.jpg',
      product_url: '/productos/test.html'
    }
  ]
}

module.exports = {
  devtool: 'source-map',
  ignore: ['**/layout.html', '**/_*', '**/.*', 'readme.md', 'yarn.lock', 'package-lock.json'],
  reshape: htmlStandards({
    //locals: (ctx) => { return { pageId: pageId(ctx), foo: 'bar' } },
    locals: (ctx) => { return json },
    minify: env === 'production'
  }),
  postcss: cssStandards({
    minify: env === 'production',
    warnForDuplicates: env !== 'production'
  }),
  babel: jsStandards(),
  vendor: 'assets/vendor/**'
}
